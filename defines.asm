; Defines for application

%define EXIT_SYSCALL 60
%define SPACE_SYMBOL 0x20
%define TAB_SYMBOL 0x9
%define NEWLINE_SYMBOL 0xA 
%define NULL_TERMINATOR_SYMBOL 0x0
%define MINUS_SYMBOL '-'
%define PLUS_SYMBOL '+'

%define PTR_SIZE 8
%define BUFFER_SIZE 256
%define STD_OUT 0x01
%define STD_ERR 0x02